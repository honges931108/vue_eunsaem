new Vue({
    el: '#vue-app',
    data: {
        a: 0,
        b: 0,
        age: 20
    },
    methods: {
        addToA1: function(){
            alert('addToA');
            return this.a + this.age;
        },
        addToB1: function(){
            alert('addToB');
            return this.b + this.age;
        }
    },
    computed: {
        addToA: function(){
            console.log('addToA');
            return this.a + this.age;
        },
        addToB: function(){
            console.log('addToB');
            return this.b + this.age;
        }
    }
});
