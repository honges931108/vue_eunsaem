var one = new Vue({
    el: '#vue-app-one',
    data: {
      title: 'Vue App One'
    },
    computed: {
      greet: function(){
        return 'Hello,from fitst :)';
      }
    }
});

var two = new Vue({
    el: '#vue-app-two',
    data: {
      title: 'Vue App Two'
    },
    computed: {
      greet: function(){
        return 'Hello,from second ';
      }
    },
    methods: {
      changeTitle: function(){
        one.title = 'Title Changed';
      }
    }
});

two.title = 'Changed from outside';
