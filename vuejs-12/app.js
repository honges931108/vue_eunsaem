new Vue({
    el: '#vue-app',
    data: {
        characters: ['aaa', 'bbb', 'cccc', 'ddd'],
        ninjas: [
            { name: 'Hong', age: 25 },
            { name: 'Park', age: 35 },
            { name: 'Kim', age: 55 }
        ]
    },
    methods: {

    },
    computed: {

    }
});
